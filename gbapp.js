var path = require("path");
var express = require("express");
var app = express();
var server = require("http").createServer(app);

var logger = require("morgan")
var bodyParser = require("body-parser") // simplifies access to request body
app.use(bodyParser.urlencoded({ extended: false }))


var a ;

var today = new Date();
// app.set("assets", path.resolve(__dirname, "assets/css")) // path to views
app.set("views", path.resolve(__dirname, "views")) // path to views
app.set("view engine", "ejs")
app.use(express.static(path.join(__dirname)));

function daysCalc(){
    // reads date in yyyy-mm-dd format
    var enteredDate = document.getElementById('datePicker').value;
    //to convert the given date to Date and store into birthDate
    var birthDate = new Date(enteredDate);
    //var test = birthDate.getHours();
    //Converting date to time and subtracting birthdate from current date
    var calcTime = today.getTime() - birthDate.getTime();
    var oneday = 24*60*60*1000; //hours*minutes*seconds*milliseconds
    //convert the returned milliseconds from calcTime to Date.
   
   
    //Calculates to number of days, referred from the below url
    //https://stackoverflow.com/questions/2627473/how-to-calculate-the-number-of-days-between-two-dates
    var calcDays = Math.round(Math.abs(calcTime/oneday));
    var calchours = calcDays*24;
    var calcMinutes = calchours*60;
    var calcSeconds = calcMinutes*60
    document.getElementById('ageDisplay').innerHTML = calcDays + '   Days';
    
   }

function hoursCalc(){
  // reads date in yyyy-mm-dd format
  var enteredDate = document.getElementById('datePicker').value;
  //to convert the given date to Date and store into birthDate
  var birthDate = new Date(enteredDate);
  //var test = birthDate.getHours();
  //Converting date to time and subtracting birthdate from current date
  var calcTime = today.getTime() - birthDate.getTime();
  var oneday = 24*60*60*1000; //hours*minutes*seconds*milliseconds
  //convert the returned milliseconds from calcTime to Date.
  

  //Calculates to number of hours, referred from the below url
  //https://stackoverflow.com/questions/2627473/how-to-calculate-the-number-of-days-between-two-dates
  var calcDays = Math.round(Math.abs(calcTime/oneday));
  var calchours = calcDays*24;
  var calcMinutes = calchours*60;
  console.log(calcTime)
  document.getElementById('ageDisplay').innerHTML = calchours + '   Hours';
 
}

function minutesCalc(){
   // reads date in yyyy-mm-dd format
   var enteredDate = document.getElementById('datePicker').value;
   //to convert the given date to Date and store into birthDate
   var birthDate = new Date(enteredDate);
   //var test = birthDate.getHours();
   //Converting date to time and subtracting birthdate from current date
   var calcTime = today.getTime() - birthDate.getTime();
   var oneday = 24*60*60*1000; //hours*minutes*seconds*milliseconds
   //convert the returned milliseconds from calcTime to Date.
  

   //Calculates to number of minutes, referred from the below url
   //https://stackoverflow.com/questions/2627473/how-to-calculate-the-number-of-days-between-two-dates
   var calcDays = Math.round(Math.abs(calcTime/oneday));
   var calchours = calcDays*24;
   var calcMinutes = calchours*60;
   var calcSeconds = calcMinutes*60
   document.getElementById('ageDisplay').innerHTML = calcMinutes + '   Minutes';

}

server.listen(8082, function () {
  console.log('listening on http://127.0.0.1:8081/')
 })

// 2 create an array to manage our entries app.locals is a built in object
var entries = []
app.locals.entries = entries // now entries can be accessed in .ejs files

// 3 set up an http request logger to log every request automagically
app.use(logger("dev"))     // app.use() establishes middleware functions
app.use(bodyParser.urlencoded({ extended: false }))


// 4 handle http GET requests (default & /new-entry)
app.get("/guestbook", function (request, response) {
  response.render("index")
})
app.get("/new-entry", function (request, response) {
  response.render("new-entry")
})

// 5 handle an http POST request to the new-entry URI 
app.post("/new-entry", function (request, response) {
  if (!request.body.title || !request.body.body) {
    response.status(400).send("Entries must have a title and a body.")
    return
  }
  entries.push({  // store it
    title: request.body.title,
    content: request.body.body,
    published: new Date()
  })
  response.redirect("guestbook")  // where to go next? Let's go to the home page :)
})

// if we get a 404 status, render our 404.ejs view
app.use(function (request, response) {
  response.status(404).render("404")
})